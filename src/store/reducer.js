import * as actionTypes from "./actions";
const initialState = {
        text: "",
        list: [],
        request:false,
        error:false
    }
;
const reducer = (state = initialState, action) => {

    if (action.type === actionTypes.CHANGER) {
        return {...state, text: action.val};

    }
    if (action.type === actionTypes.FETCH_COUNTER_REQUEST) {
        return {...state, request: true};

    }
    if (action.type === actionTypes.FETCH_COUNTER_SUCCESS) {
        let list = [];
        for (let key in action.data) {
            list.push({...action.data[key], key});
        }
        return {...state,text:"", list, request: false, error: false};

    }
    if (action.type === actionTypes.FETCH_COUNTER_ERROR) {
        console.log("ERRROORRR");
        return {...state, request: false, error: true};

    }
    return state;
};
export default reducer;