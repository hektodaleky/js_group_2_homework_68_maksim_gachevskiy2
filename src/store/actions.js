import axios from "../axios-counter";
export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_ERROR = 'FETCH_COUNTER_ERROR';
export const CHANGER = 'CHANGER';


export const changeText = event => {
    return {type: CHANGER, val: event.target.value}
};
export const saveText = (event) => {
    event.preventDefault();
    return putText();
};

export const removeEvent=(id)=>{
    return deleteText(id);

};

export const fetchCounterRequest = () => {
    return {type: FETCH_COUNTER_REQUEST}
};
export const fetchCounterSuccess = (data) => {

    return {type: FETCH_COUNTER_SUCCESS, data}
};

export const fetchCounterError = () => {
    return {type: FETCH_COUNTER_ERROR}
};

export const getData = () => {

    return (dispatch, getState) => {
        dispatch(fetchCounterRequest());
        axios.get('/todo.json').then(response => {
            dispatch(fetchCounterSuccess(response.data));

        }, error => {
            dispatch(fetchCounterError())
        })
    }
};


export const putText = () => {

    return (dispatch, getState) => {
        console.log(getState());
        dispatch(fetchCounterRequest());
        axios.post('todo.json', {text: getState().text, id: Date.now()}).then(response => {
            console.log(response);
            dispatch(getData())

        }, error => {
            dispatch(fetchCounterError())
        })
    }
};


export const deleteText = (id) =>{
return(dispatch,getState)=>{
    axios.delete(`/todo/${id}.json`).then(() => {
        console.log("Deleted!");
        dispatch(getData())
    });
}
};