import React, {Component, Fragment} from "react";
import AddNewTask from "../components/AddNewTask/AddNewTask";
import {connect} from "react-redux";
import {getData, removeEvent} from "../store/actions";
import Task from "../components/Task/Task";
import Spinner from "../components/UI/Spinner/Spinner";

class Todo extends Component {
    componentDidMount() {
        this.props.getData();
    }

    render() {
        let req;
        if (this.props.request) {
            req = <Spinner/>
        }

        else
            req = (<Fragment>
                <AddNewTask/>
                {this.props.list.map((element, index) => {
                    return (
                        <Task text={element.text} key={element.id} remove={() => this.props.removeEvent(element.key)}/>)
                })}
            </Fragment>);
        if(this.props.error)
            return <div><h1>ERRRORRRRRR!!!!!!!!!!!</h1></div>

        return (
            req
        );

    }
}
;

const mapStateToProps = state => {
    return {
        list: state.list,
        request: state.request
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getData: () => dispatch(getData()),
        removeEvent: (id) => dispatch(removeEvent(id))

    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Todo);