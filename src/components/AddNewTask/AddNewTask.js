/**
 * Created by Max on 17.01.2018.
 */
import React, {Component} from "react";
import {connect} from "react-redux";
import {changeText, saveText} from "../../store/actions";

class AddNewTask extends Component {
    render() {
        return (
            <div className="AddNewTask">
                <input onChange={this.props.changeText} value={this.props.text} placeholder="Введите задачу"
                       id="addTextInput" type="text"/>
                <button onClick={this.props.saveText}>Add</button>
            </div>
        );
    }
}


const mapStateToProps = state => {
    return {
        text: state.text,

    }
};

const mapDispatchToProps = dispatch => {
    return {
        changeText: (event) => dispatch(changeText(event)),
        saveText: (event) => dispatch(saveText(event))

    };
};
export default connect(mapStateToProps, mapDispatchToProps)(AddNewTask);